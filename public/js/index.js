/*=========== menu icon navbar ==============*/
let menu = document.querySelector("#menu-icon");
let navbar = document.querySelector(".navbar");
let close = document.querySelector(".x");

menu.onclick = () => {
  menu.classList.add("active");
  close.classList.add("active");
  navbar.classList.add("active");
};

close.onclick = () => {
  menu.classList.remove("active");
  close.classList.remove("active");
  navbar.classList.remove("active");
}; //   navbar.classList.toggle('active');
// };

/*=========== scroll sections active link ==============*/

let sections = document.querySelectorAll("section");
let navLinks = document.querySelectorAll("header nav a");

window.onscroll = () => {
  sections.forEach((sec) => {
    let top = window.scrollY;
    let offset = sec.offsetTop - 150;
    let height = sec.offsetHeight;
    let id = sec.getAttribute("id");

    if (top >= offset && top < offset + height) {
      navLinks.forEach((links) => {
        links.classList.remove("active");
        document
          .querySelector("header nav a[href*=" + id + "]")
          .classList.add("active");
      });
    }

  }
  );
  close.classList.remove("active");
  menu.classList.remove("active");

  /*=========== sticky navbar ==============*/

  let header = document.querySelector(".header");

  header.classList.toggle("sticky", window.scrollY > 100);

  /*==== remove menu icon navbar when click navbar link (scroll) =========*/
  menu.classList.remove("#x");
  navbar.classList.remove("active");
};

/*============ dark light mode ========*/

let darkModeIcon = document.querySelector("#darkMode-icon");
let whiteMode = document.querySelector(".sunlight");
let click = document.querySelectorAll(".link");



click.forEach((link) => {
  link.addEventListener("click", () => {
    menu.classList.remove("active");
    close.classList.remove("active");
  });
});

darkModeIcon.onclick = () => {
  darkModeIcon.classList.add("hidden");
  whiteMode.classList.remove("hidden");
  document.querySelector("body").classList.add("dark-mode");

  //     darkModeIcon.classList.toggle('sunlight');
  //     document.querySelector('body').classList.toggle('dark-mode');
};

whiteMode.onclick = () => {
  darkModeIcon.classList.remove("hidden");
  whiteMode.classList.add("hidden");
  document.querySelector("body").classList.remove("dark-mode");
};





















/*===== swipper section =======*/

// // /*======= menu icon navbar =========*/
// // let menuIcon = document.querySelecctor('#menu-icon');
// // let navbar = document.querySelecctor('.navbar');

// // menuIcon.onclick = () =>{
// //     menuIcon.classList.toggle('bx x');
// //     navbar.classList.toggle('active');
// // };

// // /*===== scrol sections active link =======*/
// // let sections = document.querySelectorAll("section");
// // let navLinks = document.querySelectorAll("header nav a");

// window.onscroll = () => {
//   // sections.forEach((sec) => {
//   //   let top = window.scrollY;
//   //   let offset = sec.offsetTop - 150;
//   //   let height = sec.offsetHeight;
//   //   let id = sec.getAttribute("id");

//   //   if (top >= offset && top < offset + height) {
//   //     navLinks.forEach((links) => {
//   //       links.classList.remove('active');
//   //       document.querySelector('header nav a[href*=' + id + ']').classList.add('active');
//   //     });
//   //   };
//   // });

//   /*sticky navbar*/
//   let header = document.querySelector(".header");

//   header.classList.toggle('sticky', window.scrollY > 100);

// };

// // /*========= remove menu icon navbar when click navbar link(scroll) ==========*/
// // menuIcon.classList.remove('bx x');
// //     navbar.classList.remove('active');
// // };

// /*======== dark light mode ========*/

// let darkModeIcon = document.querySelector('#darkMode-icon');

// darkModeIcon = () => {
//     darkModeIcon.classList.toggle('bx-sun');
//     document.body.classList.toggle('dar-mode');
// };

// /*========= scroll reveal =====*/
// ScrollRevreveal( {
//     reset: true,
//     distance: '80px',
//     duration: 2000,
//     delay: 200
// });

// ScrollRevreveal().reveal('.home-content, .heading', {origin: 'top'});
// ScrollRevreveal().reveal('.home-img img, .services-container, .portfolio-box, .testimonial-wrapper, .contact form', {origin: 'bottom'});

// ScrollRevreveal().reveal('.home-contact h1, .about-img img', {origin: 'left'});

// ScrollRevreveal().reveal('.home-contact h3, .home-content p, .about-content p, .about-content', {origin: 'right'});
